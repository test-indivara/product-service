package com.example.productservice.service;

import com.example.productservice.model.Product;
import com.example.productservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<Product> findAll(){
        return productRepository.findAll();
    }

    public Product findById(Long id){
        Optional<Product> optProduct = productRepository.findById(id);
        Product product = new Product();
        if(optProduct.isPresent()){
            product = optProduct.get();
        }
        return product;
    }

    public List<Product> findByName(String name){
        return productRepository.findByName(name);
    }

    public Product saveProduct(Product product){
        return productRepository.save(product);
    }
}
