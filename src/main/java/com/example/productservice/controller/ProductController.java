package com.example.productservice.controller;

import com.example.productservice.model.Product;
import com.example.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/list")
    List<Product> findAll(){
        return productService.findAll();
    }

    @GetMapping("/{id}")
    Product findById(@PathVariable() Long id){
        return productService.findById(id);
    }

    @GetMapping()
    List<Product> findByName(@RequestParam String name){
        return productService.findByName(name);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    Product saveProduct(@RequestBody Product product){
        return productService.saveProduct(product);
    }
}
